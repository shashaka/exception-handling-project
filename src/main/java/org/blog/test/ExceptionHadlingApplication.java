package org.blog.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExceptionHadlingApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExceptionHadlingApplication.class, args);
    }}