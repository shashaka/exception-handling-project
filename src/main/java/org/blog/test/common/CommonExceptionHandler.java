package org.blog.test.common;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.ResourceAccessException;

@ControllerAdvice
public class CommonExceptionHandler {

    @ExceptionHandler(value = InternalError.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST,
            reason = "internal error exception")
    public void InternalErrorException() {
    }
}
