package org.blog.test.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;

@RestController
public class TestController {

    @GetMapping(value = "/test")
    public void getTestString() {
        throw new InternalError();
    }
}
